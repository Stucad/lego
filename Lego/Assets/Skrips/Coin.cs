using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Coin : MonoBehaviour
{
    private int score;
    public TextMeshProUGUI scoreText;

    private void Start()
    {
        score = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Moneda")
        {
            score++;
            scoreText.text = "" + score;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Move : MonoBehaviour
{
    public Animator anim;

    [Range(0, 10)]
    Rigidbody rb;
    bool jumpFloor = false;
    bool jumpTru = false;
    public float jumpForce = 5f;
    public float moveSpeed;
    public float rotationSpeed;
    public AudioClip audioFx;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("Speed", moveDirection.z);
        transform.Translate((moveDirection * moveSpeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotationSpeed,0));

        if(moveSpeed <= 5 && Input.GetButtonDown("Shift"))
        {
            moveSpeed = 10;
        }
        if(moveSpeed >=5 && Input.GetButtonUp("Shift"))
        {
            moveSpeed = 5;
        }

        Vector3 floor = transform.TransformDirection(Vector3.down);

        if(Physics.Raycast(transform.position,floor,0.5f))
        {
            jumpFloor = true;
           
        }
        else
        {
            jumpFloor = false;
        }
        jumpTru = Input.GetButtonDown("Jump");

        if(jumpTru && jumpFloor)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            AudioSource.PlayClipAtPoint(audioFx, gameObject.transform.position);
        }
    }
}

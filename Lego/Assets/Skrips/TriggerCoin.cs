using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCoin : MonoBehaviour
{
    public AudioClip audioFx;

    private void OnTriggerEnter(Collider collition)
    {
        if(collition.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(audioFx, gameObject.transform.position);
            if(audioFx == true)
            {
                Destroy(this.gameObject);
            }

        }
    }
}
